#!/bin/bash

###
#
# Windigo's MPD Script
#
###

case $1 in
        next)
		# Move to next song, save details in $SONG
                MPD_TITLE=`mpc -f '%title%' next | head -n 1`
	;;

        prev)
		# Move to previous song, save details in $SONG
                MPD_TITLE=`mpc -f '%title%' prev | head -n 1`

	;;

        toggle)
		# Toggle playback
                mpc toggle > /dev/null

		exit 0
	;;

        stop)
		# Quit playing stuff
                mpc stop > /dev/null

		exit 0
        ;;

        *)
		# Print a usage reminder
                echo "Usage: mpd.sh [ toggle | stop | next | prev ]"

		# That's right, seven, bitches.
                exit 7
        ;;
esac

# Display a notification, for 150 milliseconds, with an audio icon,
#  that shows the song title and band name MAN THIS IS FUCKING COOL
notify-send -u low -t 50 -i notification-audio-volume-high "$MPD_TITLE" "`mpc -f '%artist%' current`"
